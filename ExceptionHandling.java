import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            //edit here
            try{

                System.out.println("Masukan pembilang!");
                int pembilang = scanner.nextInt();

                System.out.println("Masukan penyebut!");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println(pembilang + ":" + penyebut + "=" + hasil);

                validInput = true;

            }

            catch(InputMismatchException e){
                System.out.println("Bilangan yang di input harus bilangan bulat. Silahkan coba lagi!");
            }
            
            catch(ArithmeticException e){
                System.out.println("Bilangan tidak dapat di input.");
            }

        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0

        if (penyebut == 0){
            throw new ArithmeticException("Tidak dapat di input!!!!");
        }

        return pembilang / penyebut;
    }
}
